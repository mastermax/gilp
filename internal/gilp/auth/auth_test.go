package auth

import (
	"net/http"
	"strings"
	"testing"

	"gitlab.com/johanngyger/gilp/internal/gilp/test"

	"github.com/rs/zerolog/log"
)

func TestAuthorizeRequestMissingBasicAuth(t *testing.T) {
	req, _ := http.NewRequest("GET", "/foo", nil)

	err := AuthorizeRequest(req, test.JWKS)
	log.Error().Err(err)

	if err == nil || err.Error() != "invalid basic auth header" {
		t.Errorf("Got another error: %v", err)
	}
}

func TestAuthorizeRequestInvalidJWT(t *testing.T) {
	req, _ := http.NewRequest("GET", "/foo", nil)
	req.SetBasicAuth("foo", "bar")

	err := AuthorizeRequest(req, test.JWKS)
	log.Error().Err(err)

	if err == nil || !strings.HasPrefix(err.Error(), "claims problem in JWT: parse JWT token: ") {
		t.Errorf("Got another error: %v", err)
	}
}

func TestAuthorizeRequestMissingExpirationTimeClaim(t *testing.T) {
	req, _ := http.NewRequest("GET", "/foo", nil)
	jwt := "eyJraWQiOiJ0ZXN0IiwiYWxnIjoiUlMyNTYiLCJ0eXAiOiJKV1QifQ.eyJzdWIiOiJqb2JfMTIzNDU2IiwibmFtZXNwYWNlX2lkIjoiMTIzNDU2In0.dNjpzJJE81boayIL08xPRmqsB6xkb_Y0QI9oO5WkOyB6yyqn4Rw2gTRIBcwBni_MaOd3rfaL0Q1Koc4ahteLOqpYuBOHfyt-O4RtLsPST-V553eQq4zQ_EBUB7NgKkk9kW-NZwmsxkGZi7f4UbIestfPV27hk1k8Z7i_J11zDL_-uH-oQw0m4wPe2xTTX2Uq9rptilzSe-UQCZERoOOf36w-BqseKudCVAMPkgiFEKmG5T1VIGfpvUNTZHjI3ryQtBcCDKDXT0i4Vl41xob8VGN3lL6yWYhyIJlvphRqkqI_NVlDyQEa-nPevmOllgDGoNpCcd3vo8QUY9Kjg2xJHw"
	req.SetBasicAuth("foo", jwt)

	err := AuthorizeRequest(req, test.JWKS)
	log.Error().Err(err)

	if err == nil || err.Error() != "missing expiration time claim in JWT" {
		t.Errorf("Got another error: %v", err)
	}
}

func TestAuthorizeRequestExpiredJWT(t *testing.T) {
	req, _ := http.NewRequest("GET", "/foo", nil)
	jwt := "eyJraWQiOiJ0ZXN0IiwiYWxnIjoiUlMyNTYiLCJ0eXAiOiJKV1QifQ.eyJleHAiOjE2MDY5NDI1OTcsInN1YiI6ImpvYl8xMjM0NTYiLCJuYW1lc3BhY2VfaWQiOiIxMjM0NTYiLCJuYW1lc3BhY2VfcGF0aCI6Ii9mb28vYmFyIn0.kucqmpGhRvKz1w-XGoxU20ZWQ2F42n9Wdn0mWTOlFimLj1GWmkok2a_JFO2CApufCV_cOAUKuKwVS9ZOD_BnbBN7hLWkoEcGUtUKGetu05dlAnCm-ihCVjLoptKfjrriFizg7tOIgu5q-1xQkR-hjXroM18AMuqKjEDFmaWXvBSWX9DgKhMN_XiTsLpYqdeYzENId9eZT1xC-oNwakdu4CjripSLu5UzfH1PjFPPrZb-TfhpJnvXfB1S5VW64g23RxHc_o8zHaLwKRMPXTN8CoOnnR7TnzJq9Pw9YybsXuCYZqaCEeHMXlFTfOai_i5he8b3VFuUWBkbpxOrZGt1wQ"
	req.SetBasicAuth("foo", jwt)

	err := AuthorizeRequest(req, test.JWKS)
	log.Error().Err(err)

	if err == nil || !strings.HasPrefix(err.Error(), "JWT expired: ") {
		t.Errorf("Got another error: %v", err)
	}
}

func TestAuthorizeRequestMissingNamespace(t *testing.T) {
	req, _ := http.NewRequest("GET", "/foo", nil)
	jwt := "eyJraWQiOiJ0ZXN0IiwiYWxnIjoiUlMyNTYiLCJ0eXAiOiJKV1QifQ.eyJleHAiOjI2MDY5NDI1OTcsInN1YiI6ImpvYl8xMjM0NTYiLCJuYW1lc3BhY2VfaWQiOiIxMjM0NTYifQ.RNw48nmpgwatS1OM8DvMW6tANxfwMSuxRKJZhrdfsi3aL87kJsrGyl6fqm1_U7w8NbQWr_FJP7cvD9ojQV-rbkdPpFkR3RE-ArjdnwuSqC19ePINe3ZvxiRmjXaqcHVXFTz39P0jiKROktGhy7KujX6-K6J7o1kj940yMqVvFifozwWuwegHh6MLpnqtahuH1ZZaIk4Gw3EQxLhDqU_dCIsC-jZyV91hLi2fI-SaUzr1zOd8v9Rq43IZ5YRwhe-iSFDYJWVRuas9It1JdtGtyV6GoE7mWn-pYVXSlAxuZB0_enYyxrDwNP-YXgemwIQqT3jrq9sHiwRqbqNna8FwCQ"
	req.SetBasicAuth("foo", jwt)

	err := AuthorizeRequest(req, test.JWKS)
	log.Error().Err(err)

	if err == nil || err.Error() != "missing namespace_path claim in JWT" {
		t.Errorf("Got another error: %v", err)
	}
}

func TestAuthorizeRequestOtherPathGET(t *testing.T) {
	req, _ := http.NewRequest("GET", "/baz", nil)
	req.SetBasicAuth("foo", test.JWT)

	err := AuthorizeRequest(req, test.JWKS)
	log.Error().Err(err)

	if err != nil {
		t.Errorf("Request must be authorized: %v", err)
	}
}

func TestAuthorizeRequestAPIPOSTSomeProject(t *testing.T) {
	req, _ := http.NewRequest("POST", "/api/v4/projects/somegroup%2Fsomesubgroup%2Fsomeproject/repository/tags", nil)
	req.Header.Set("PRIVATE-TOKEN", test.JWT)

	err := AuthorizeRequest(req, test.JWKS)
	log.Error().Err(err)

	if err != nil {
		t.Errorf("Request must be authorized: %v", err)
	}
}

func TestAuthorizeRequestAPIInvalidJWT(t *testing.T) {
	req, _ := http.NewRequest("GET", "/api/v4/version", nil)
	req.Header.Set("PRIVATE-TOKEN", "")

	err := AuthorizeRequest(req, test.JWKS)
	log.Error().Err(err)

	if err == nil || !strings.HasPrefix(err.Error(), "claims problem in JWT: ") {
		t.Errorf("Got another error: %v", err)
	}
}

func TestAuthorizeRequestAPIGET(t *testing.T) {
	req, _ := http.NewRequest("GET", "/api/v4/version", nil)
	req.Header.Set("PRIVATE-TOKEN", test.JWT)

	err := AuthorizeRequest(req, test.JWKS)

	if err != nil {
		t.Errorf("Request must be authorized: %v", err)
	}
}

func TestAuthorizeRequestValidJWT(t *testing.T) {
	req, _ := http.NewRequest("GET", "/foo/bar/baz", nil)
	req.SetBasicAuth("foo", test.JWT)

	err := AuthorizeRequest(req, test.JWKS)

	if err != nil {
		t.Errorf("Request must be authorized: %v", err)
	}
}
