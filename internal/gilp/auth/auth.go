// Package auth handles http.Requests and authorizes them based on their JWT credentials.
package auth

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	myjwt "gitlab.com/johanngyger/gilp/internal/gilp/jwt"

	"github.com/rs/zerolog/log"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
)

// AuthorizeRequest authorizes requests to gitlab instance.
func AuthorizeRequest(req *http.Request, jwks *jose.JSONWebKeySet) error {
	log.Info().Msgf("Request %s %s from %s", req.Method, req.URL, req.Header.Get("X-Forwarded-For"))

	if strings.HasPrefix(req.URL.Path, "/api") {
		return authorizeAPIRequest(req, jwks)
	} else {
		return authorizeGitRequest(req, jwks)
	}
}

func authorizeGitRequest(req *http.Request, jwks *jose.JSONWebKeySet) error {
	_, token, ok := req.BasicAuth()
	if !ok {
		return errors.New("invalid basic auth header")
	}

	_, _, err := validateJWT(token, jwks)
	if err != nil {
		return err
	}

	// This is the place to add custom authorization logic

	return nil
}

func authorizeAPIRequest(req *http.Request, jwks *jose.JSONWebKeySet) error {
	// For API requests support the PRIVATE-TOKEN header
	// https://docs.gitlab.com/ce/api/README.html#personalproject-access-tokens
	token := req.Header.Get("PRIVATE-TOKEN")

	_, _, err := validateJWT(token, jwks)
	if err != nil {
		return err
	}

	if req.Method == "GET" {
		return nil
	}

	// This is the place to add custom authorization logic

	return nil
}

func validateJWT(jwt string, jwks *jose.JSONWebKeySet) (*jwt.Claims, *myjwt.GitLabClaims, error) {
	claims, gitLabClaims, err := myjwt.ExtractClaims(jwt, jwks)
	if err != nil {
		return nil, nil, fmt.Errorf("claims problem in JWT: %w", err)
	}

	log.Info().Msgf("JWT claims: %+v, %+v", claims, gitLabClaims)

	if claims.Expiry == nil {
		return nil, nil, errors.New("missing expiration time claim in JWT")
	}

	expiration := claims.Expiry.Time()
	now := time.Now()
	if now.After(expiration) {
		return nil, nil, fmt.Errorf("JWT expired: now=%v, expiration=%v", now.Format(time.RFC3339), expiration.Format(time.RFC3339))
	}

	nsPath := gitLabClaims.NamespacePath
	if nsPath == "" {
		return nil, nil, errors.New("missing namespace_path claim in JWT")
	}

	return claims, gitLabClaims, nil
}
