// Package jwt handles JSON web tokens (JWT).
package jwt

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/rs/zerolog/log"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"

	"gitlab.com/johanngyger/gilp/internal/gilp/config"
)

var GitLabJWKS jose.JSONWebKeySet

func Load() error {
	resp, err := http.Get(config.General.GitLabURL + "/-/jwks")
	if err != nil {
		return fmt.Errorf("Could not get %s/-/jwks: %w", config.General.GitLabURL, err)
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Error().Err(err).Msg("Error closing request body.")
		}
	}()

	gitLabJWKSBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("Could not parse response of %s/-/jwks: %w", config.General.GitLabURL, err)
	}

	err = json.Unmarshal(gitLabJWKSBytes, &GitLabJWKS)
	if err != nil {
		return fmt.Errorf("Could not unmarshal GitLab JWKS: %w", err)
	}

	return nil
}

/*
Sample JWT payload
https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/#how-it-works

{
  "jti": "c82eeb0c-5c6f-4a33-abf5-4c474b92b558", # Unique identifier for this token
  "iss": "gitlab.example.com",                   # Issuer, the domain of your GitLab instance
  "iat": 1585710286,                             # Issued at
  "nbf": 1585798372,                             # Not valid before
  "exp": 1585713886,                             # Expire at
  "sub": "job_1212",                             # Subject (job id)
  "namespace_id": "1",                           # Use this to scope to group or user level namespace by id
  "namespace_path": "mygroup",                   # Use this to scope to group or user level namespace by path
  "project_id": "22",                            #
  "project_path": "mygroup/myproject",           #
  "user_id": "42",                               # Id of the user executing the job
  "user_login": "myuser"                         # GitLab @username
  "user_email": "myuser@example.com",            # Email of the user executing the job
  "pipeline_id": "1212",                         #
  "job_id": "1212",                              #
  "ref": "auto-deploy-2020-04-01",               # Git ref for this job
  "ref_type": "branch",                          # Git ref type, branch or tag
  "ref_protected": "true"                        # true if this git ref is protected, false otherwise
}
*/

// See https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/ for documentation of the payload
type GitLabClaims struct {
	NamespaceID   string `json:"namespace_id,omitempty"`
	NamespacePath string `json:"namespace_path,omitempty"`
	ProjectID     string `json:"project_id,omitempty"`
	ProjectPath   string `json:"project_path,omitempty"`
	UserID        string `json:"user_id,omitempty"`
	UserLogin     string `json:"user_login,omitempty"`
	UserEmail     string `json:"user_email,omitempty"`
	PipelineID    string `json:"pipeline_id,omitempty"`
	JobID         string `json:"job_id,omitempty"`
	Ref           string `json:"ref,omitempty"`           // Git ref for this job
	RefType       string `json:"ref_type,omitempty"`      // Git ref type: branch or tag
	RefProtected  string `json:"ref_protected,omitempty"` // true if this git ref is protected, false otherwise
}

// ExtractClaims extracts the claims from the JWT in tokenString.
func ExtractClaims(tokenString string, jwks *jose.JSONWebKeySet) (*jwt.Claims, *GitLabClaims, error) {
	claims := &jwt.Claims{}
	gitLabClaims := &GitLabClaims{}

	token, err := jwt.ParseSigned(tokenString)
	if err != nil {
		return nil, nil, fmt.Errorf("parse JWT token: %w", err)
	}

	err = token.Claims(jwks, &claims, &gitLabClaims)
	if err != nil {
		return nil, nil, fmt.Errorf("JWT claims: %w", err)
	}

	return claims, gitLabClaims, nil
}
