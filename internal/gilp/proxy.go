package gilp

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/http/httputil"
	"strings"

	"github.com/rs/zerolog/log"
	"gitlab.com/johanngyger/gilp/internal/gilp/auth"
	"gitlab.com/johanngyger/gilp/internal/gilp/config"
	"gitlab.com/johanngyger/gilp/internal/gilp/jwt"
)

func Start() {
	err := config.Load()
	if err != nil {
		log.Error().Err(err).Msg("Error while loading config.")
		return
	}

	err = jwt.Load()
	if err != nil {
		log.Error().Err(err).Msg("Error initializing JWT module.")
		return
	}

	startProxy()
}

func startProxy() {
	proxy := httputil.NewSingleHostReverseProxy(config.GitLabURL)

	http.HandleFunc("/", logHandler(func(w http.ResponseWriter, req *http.Request) {
		handleProxyRequest(w, req, proxy)
	}))

	listen := fmt.Sprintf(":%d", config.General.Port)
	log.Info().Msgf("Listening on %s and forwarding requests to -> %s", listen, config.GitLabURL)
	err := http.ListenAndServe(listen, nil)
	if err != nil {
		log.Error().Err(err).Msg("Could not start HTTP server")
		return
	}

}

func handleProxyRequest(w http.ResponseWriter, req *http.Request, proxy *httputil.ReverseProxy) {
	err := auth.AuthorizeRequest(req, &jwt.GitLabJWKS)
	if err != nil {
		log.Info().Err(err).Msg("Request not authorized")
		w.Header().Set("WWW-Authenticate", "Basic")
		http.Error(w, "401 Unauthorized", http.StatusUnauthorized)
		return
	}

	req.URL.Host = config.GitLabURL.Host
	req.Host = config.GitLabURL.Host
	req.URL.Scheme = config.GitLabURL.Scheme

	if strings.HasPrefix(req.URL.Path, "/api") {
		req.Header.Set("PRIVATE-TOKEN", config.General.Token)
	} else {
		req.SetBasicAuth("", config.General.Token)
	}

	if config.Debug.DumpRequests {
		x, _ := httputil.DumpRequest(req, false)
		log.Printf("===== PROXY REQUEST DUMP TO GITLAB ===== %q", x)
	}

	proxy.ServeHTTP(w, req)
}

func logHandler(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if !config.Debug.DumpRequests {
			fn(w, r)
			return
		}

		x, err := httputil.DumpRequest(r, false)
		if err != nil {
			log.Error().Msgf("Failed to dump request: %v", r)
			http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
			return
		}

		log.Printf("<<<<< REQUEST DUMP <<<<< %q", x)

		rec := httptest.NewRecorder()
		fn(rec, r)

		log.Printf(">>>>> RESPONSE DUMP >>>>> %q", rec.Body)

		// this copies the recorded response to the response writer
		for k, v := range rec.Header() {
			w.Header()[k] = v
		}
		w.WriteHeader(rec.Code)
		_, _ = rec.Body.WriteTo(w)
	}
}
