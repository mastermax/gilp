FROM golang AS builder
WORKDIR /src
COPY . .
RUN go test ./...
RUN CGO_ENABLED=0 go build

FROM alpine
WORKDIR /gilp
COPY --from=builder /src/gilp .
CMD ["./gilp"]
